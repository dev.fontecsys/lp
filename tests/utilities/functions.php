
<?php

function create($class, $attributes = [])
{
    return factory($class)->create($attributes);
}
function createMany($class, $attributes ,$times=1)
{
    return factory($class,$times)->create($attributes);
}
function make($class, $attributes = [])
{
    return factory($class)->make($attributes);
}
