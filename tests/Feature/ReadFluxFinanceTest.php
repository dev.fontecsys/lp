<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReadFluxFinanceTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->withoutExceptionHandling();


    }

    /** @test */
    function  un_utilisateur_non_authentifie_ne_peut_lire_les_flux_de_finances()
    {

        $this->withoutExceptionHandling();

        $this->expectException('Illuminate\Auth\AuthenticationException');

        $this->json('get','/api/flux_finances');
    }
    /** @test */
    function  un_utilisateur_authentifie_peut_lire_les_flux_de_finances()
    {

        $this->signInAPI();
        $this->withoutExceptionHandling();

        //given we have locations
        $location = create('App\Location',['tarif'=>25000]);
        //$location = create('App\Location',['tarif'=>35000]);


        $response =$this->json('get','/api/flux_finances');

        $response->assertStatus(200);
    }
}
