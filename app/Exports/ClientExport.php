<?php

namespace App\Exports;

use App\client;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\Exportable;


class ClientExport implements WithTitle, FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize
{
    use Exportable; 

    private $search;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($search)
    {
        $this->search =$search;
    }
    
    public function title(): string
    {
        return 'clients';
    }
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return client::query()->search($this->search);
    }


 
    //use Exportable; 

    public function map($client): array
    {
        return [
            $client->nom,
            $client->prenom,
            $client->adresse,
            $client->telephone1

        ];
    }

    public function headings(): array
    {
        return [
            "Nom ",
            "prenom",
            'adresse',
            'telephone'
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}
