<?php

namespace App\Exports;

use App\Chauffeur;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ChauffeurExport implements FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize,WithTitle
{

    use Exportable; 

    private $search;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($search)
    {
        $this->search =$search;
    }

    public function title(): string
    {
        return 'Tous les chauffeurs';
    }
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Chauffeur::query()->search($this->search);
    }


 
    //use Exportable; 

    public function map($chauffeur): array
    {
        return [
            $chauffeur->prenom." ".$chauffeur->nom,
            $chauffeur->type_permis_conduire,
            $chauffeur->nr_permis_conduire,
            $chauffeur->telephone1,
            $chauffeur->email,
            $chauffeur->adrese,

        ];
    }

    public function headings(): array
    {
        return [
            "Identité",
            'Type de permis',
            'N° de permis',
            'N° de téléphone',
            'E-mail',
            'Adresse',
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}
