<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Modele extends SModel
{
    use LogsActivity;

    protected static $logAttributes = ["pays_id","libelle","marque_id"];
    protected static $logName = 'marque';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;
    

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé le modèle <strong>{$this->libelle}</strong> de la marque <strong>{$this->marque->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé le modèle <strong>{$this->libelle}</strong> de la marque <strong>{$this->marque->libelle}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié le modèle <strong>{$this->libelle}</strong> de la marque <strong>{$this->marque->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié le modèle <strong>{$this->libelle}</strong> de la marque <strong>{$this->marque->libelle}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté le modèle <strong>{$this->libelle}</strong> à la marque  <strong>{$this->marque->libelle}</strong> ";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté le modèle <strong>{$this->libelle}</strong> à la marque <strong>{$this->marque->libelle}</strong>";
        }
        
    }
    public function marque()
    {
        return $this->belongsTo("App\Marque");

    }
}
