<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;


class Depense extends SModel
{
    
    use LogsActivity;

    protected static $logAttributes = ['vehicule_id', 'type_depense_id','date_facturation','montant','description'];
    protected static $logName = 'depense';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé une dépense <strong>{$this->libelle}</strong> de la voiture<strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé une dépense <strong>{$this->libelle}</strong> de la voiture<strong>{$this->vehicule->libelle}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié une dépense <strong>{$this->libelle}</strong> de la voiture<strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié une dépense <strong>{$this->libelle}</strong> de la voiture<strong>{$this->vehicule->libelle}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté une dépense <strong>{$this->libelle}</strong> de <strong>{$this->montant}</strong> F cfa  à la voiture <strong>{$this->vehicule->libelle}</strong> ";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté une dépense <strong>{$this->libelle}</strong> de <strong>{$this->montant}</strong> F cfa à la voiture<strong>{$this->vehicule->libelle}</strong>";
        }
        
    }
    protected $appends = [];

    public function vehicule()
    {
        return $this->belongsTo("App\Vehicule");
    }

    public function type_depense()
    {
        return $this->belongsTo("App\TypeDepense");
    }

    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('depenses.description', 'LIKE', "%{$q}%")
                ->orWhere('depenses.montant', 'LIKE', "%{$q}%")
                ->orWhere('vehicules.libelle', 'LIKE', "%{$q}%")
                ->orWhere('type_depenses.libelle', 'LIKE', "%{$q}%")
                ->leftJoin('vehicules', 'vehicules.id', '=', 'depenses.vehicule_id')
                ->leftJoin('type_depenses', 'type_depenses.id', '=', 'depenses.type_depense_id');
    }
}
