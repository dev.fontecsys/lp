<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Marque extends SModel
{
    use LogsActivity;

    protected static $logAttributes = ["pays_id","libelle"];
    protected static $logName = 'marque';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;
    

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé la marque {$this->libelle}";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé la marque {$this->libelle}";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié la marque {$this->libelle}";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié la marque {$this->libelle}";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté la marque {$this->libelle}";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté la marque {$this->libelle}";
        }
        
    }

    public function pays()
    {
        return $this->belongsTo("App\Pays");

    }


    public function modeles()
    {
        return $this->hasMany("App\Modele");

    }



    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('libelle', 'LIKE', "%{$q}%")
                ->orWhere('created_at', 'LIKE', "%{$q}%");
    }

}
