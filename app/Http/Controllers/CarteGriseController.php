<?php

namespace App\Http\Controllers;

use App\CarteGrise;
use Illuminate\Http\Request;

class CarteGriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarteGrise  $carteGrise
     * @return \Illuminate\Http\Response
     */
    public function show(CarteGrise $carteGrise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarteGrise  $carteGrise
     * @return \Illuminate\Http\Response
     */
    public function edit(CarteGrise $carteGrise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarteGrise  $carteGrise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarteGrise $carteGrise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarteGrise  $carteGrise
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarteGrise $carteGrise)
    {
        //
    }
}
