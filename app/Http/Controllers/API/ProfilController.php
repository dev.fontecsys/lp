<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Models\Activity;

class ProfilController extends Controller
{


    public function index()
    {
        $user = User::whereId(Auth::user()->id)->first();
        if($user)
        {
            return response()->json(['success' => true,'user'=>$user->load(['role'])],200);
        }
        else
        {
            return response()->json(['success' => false],200);
        }
    }

    public function activities()
    {
        return Activity::with(['causer','subject'])->where('causer_id',Auth::user()->id)->orderBy('created_at','desc')->paginate(10);
    }

    public function password(Request $request)
    {
        $validatedData = $request->validate([
            'current' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ],
        [
            "current.required"=>"Le mot de passe actuel est requis",
            "password.required"=>"Le nouveau mot de passe  est requis",
            "password_confirmation.required"=>"La confirmation du  mot de passe est requise",
            "password.confirmed"=>"Le nouveau mot de passe et la confirmation ne correspondent pas",
        ]
    );

        $user = User::find(Auth::id());

        if (!Hash::check($request->current, $user->password)) {
            return response()->json(['errors' => ['current'=> ['Le mot de passe actuelle ne correspond pas']]], 422);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(['success' => true,"user"=>$user->fresh()->load('role')], 200);

    }

    public function avatar(Request $request)
    {
        $validatedData = $request->validate([
            'avatar' => 'required'
        ],
        [
            "avatar.required"=>"L'avatar de votre profil est requis",
        ]
      );

        $user = Auth::user();
        //on met a jour l image
        $file = $request->input('avatar');
        $user->upload($file,$user->getMime($file),$user->id,$user->avatar!=null? $user->avatar : "");
        
      return response()->json(['success' => true,"avatar"=>$user->avatar], 200);

    }


    public function info(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
                'email' => 'required|email|max:50|unique:users,email,'.$user->id,
                'name' => 'required|max:20',
                'description' => 'max:100',
            ],
            [
                "name.required"=>"Le nom est requis",
                "name.max"=>"Le nom ne doit pas avoir plus de 20 caractères",
                "email.max"=>"L'e-mail ne doit pas avoir plus de 50 caractères",
                "description.max"=>"La description ne doit pas avoir plus de 100 caractères",
                "email.required"=>"L'e-mail est requis",
                "email.unique"=>"Cet e-mail est déjà pris",
                "email.email"=>"Le format de l'e-mail donné est incorrect",
            ]
        );
        
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->description = $request->input('description');
        $user->save();

        return response()->json(['success' => true,"user"=>$user->fresh()->load('role')], 200);
    }
}
