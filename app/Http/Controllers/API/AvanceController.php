<?php

namespace App\Http\Controllers\API;

use App\FluxFinance;
use App\Http\Controllers\Controller;
use App\Location;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AvanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            "location_id"=>"required|exists:locations,id",
            "date_transaction"=>"required|date",
            'montant' => ['required','numeric','min:0',function($attribute,$value,$fail) use ($request)
            {
              if($value>$request->input('reste_a_payer'))
              {
                $fail('Le montant ne doit pas déppasser le reste à payer');
              }
            }]
        ],
        [
            "montant.required"=>"Le montant est requis",
            "montant.numeric"=>"Le montant doit être un nombre",
            "montant.min"=>"Le montant doit être positif",
            "date_transaction.required"=>"La date du versement  est obligatoire",
            "date_transaction.date"=>"La date du versement  doit avoir un format de date"
        ]);

        $location = Location::whereId($request->input('location_id'))->first();

        $jours = ($location->nbr_jour > 1) ? "jours":"jour";
        $prefix1 =  ($location->statutPayement->nature=="payé") ? "Paiement total" : (($location->statutPayement->nature=="avance") ? "Avance sur paiement" : "" );
        $prefix2 = $location->parent_id!=null ? " : Prolongement n°".$location->numero : ": Location n° ".$location->numero ;
        $description =$prefix1." ".$prefix2." pour ".$location->nbr_jour." ".$jours." : Du "
                      .Carbon::parse($location->date_dbt)->format('d-m-Y')." au "
                      .Carbon::parse($location->date_fin)->format('d-m-Y');

        $flux = FluxFinance::create(
            [
              "flux"=> $description,
              "montant"=> $request->input('montant'),
              "financiable_id"=> $location->id,
              "vehicule_id"=> $location->vehicule_id,
              "date_transaction"=>Carbon::parse($request->input('date_transaction'))->format('Y-m-d'),
              "financiable_type"=>"App\Location",
            

            ]);
            return response()->json(["succes"=>true,'flux'=>$flux],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            "location_id"=>"required|exists:locations,id",
            'montant' => ['required','numeric','min:0',function($attribute,$value,$fail) use ($request)
            {
              $seuil = doubleval($request->input('reste_a_payer'))+ doubleval($request->input('ancien_montant'));
              if($value>$seuil)
              {
                $fail('Le montant ne doit pas dépasser la limite de '.$seuil.' F cfa');
              }
            }]
        ],
        [
            "montant.required"=>"Le montant est requis",
            "montant.numeric"=>"Le montant doit être un nombre",
            "montant.min"=>"Le montant doit être positif"
        ]);

        $location = Location::whereId($request->input('location_id'))->first();

        $jours = ($location->nbr_jour > 1) ? "jours":"jour";
        $prefix1 =  ($location->statutPayement->nature=="payé") ? "Paiement total" : (($location->statutPayement->nature=="avance") ? "Avance sur paiement" : "" );
        $prefix2 = $location->parent_id!=null ? " : Prolongement n°".$location->numero : ": Location n° ".$location->numero ;
        $description =$prefix1." ".$prefix2." pour ".$location->nbr_jour." ".$jours." : Du "
                      .Carbon::parse($location->date_dbt)->format('d-m-Y')." au "
                      .Carbon::parse($location->date_fin)->format('d-m-Y');

        $flux = FluxFinance::whereId($request->input('id'))->first();
        $flux->montant = $request->input('montant'); 
        $flux->date_transaction = $request->input('date_transaction');  
        $flux->save();  
        
        return response()->json(["succes"=>true,'flux'=>$flux->fresh(),"entity"=>$location->fresh()],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $flux = FluxFinance::whereId($id)->first();
        $flux->delete();
    }
}
