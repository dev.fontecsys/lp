<?php

namespace App\Http\Controllers\API;

use App\Vehicule;
use App\Location;
use App\Client;
use App\FluxFinance;
use App\Historique;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ViewRecords\VehicleRecord;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dateEncours = Carbon::now();
        $anneeEncours = $dateEncours->year;


        //Chiffre d'affaire de l'année en cours
        $ca = DB::table("flux_finances")->selectRaw("sum( case when flux_finances.montant is not null and INSTR(trim(flux_finances.financiable_type),'Location') then flux_finances.montant else 0 end) as entree, sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end) as sortie, (abs(sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Location') then flux_finances.montant else 0 end) - sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end))) as result")
        ->whereRaw("YEAR(date_transaction) = {$anneeEncours}")->get()->first();

        //Voiture la plus prolifique
        $vpp=DB::table("vehicules")->selectRaw("distinct vehicules.libelle,vehicules.id,plaque_immatriculation,(sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Location') then flux_finances.montant else 0 end) - sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end)) as result")
        ->leftJoin("flux_finances","flux_finances.vehicule_id","=","vehicules.id")
        ->orderBy('result','desc')
        ->having('result', '>', 0)
        ->groupBy("libelle","plaque_immatriculation","vehicules.id")->get()->first();
        //Voiture la moins prolifique
        $vmp=DB::table("vehicules")->selectRaw("distinct vehicules.libelle,vehicules.id,plaque_immatriculation,(sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Location') then flux_finances.montant else 0 end) - sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end)) as result")
        ->leftJoin("flux_finances","flux_finances.vehicule_id","=","vehicules.id")
        ->orderBy('result','asc')
        ->groupBy("libelle","plaque_immatriculation",'vehicules.id')->get()->first();
        //Meilleurs clients
        $cpp=DB::table("clients")->selectRaw("distinct clients.id, clients.nom,clients.prenom,sum( case when flux_finances.montant is not null  then flux_finances.montant else 0 end) as result")
        ->leftJoin("locations","locations.client_id","=","clients.id")
        ->leftJoin("flux_finances","flux_finances.financiable_id","=","locations.id")
        ->whereRaw("INSTR (trim(flux_finances.financiable_type),'Location')")
        ->orderBy('result','desc')
        ->having('result', '>', 0)
        ->groupBy("clients.id","clients.nom","clients.prenom")->get()->first();

         $loc = Location::with(['client','vehicule'])->orderBy('created_at','desc')->take(10)->get();
         $cc = $loc->count() == 0  ? 1 *4 : $loc->count() *4;

        return [
            "ca"=>$ca,
            "vpp"=>$vpp,
            "cpp"=>$cpp,
            "vmp"=>$vmp,
            "vehicules_count" => DB::table('vehicules')->count(),
            "historiques" =>Historique::with(['causer','subject'])->orderBy('activity_log.created_at','desc')->take(intVal($cc))->get(),
            "clients_count" => DB::table('clients')->count(),
            "locations_count" => DB::table('locations')->count(),
            "locations_en_cours_count" => DB::table('locations')->where("date_dbt","<=",$dateEncours->toDateTimeString())->where("date_fin",">=",$dateEncours->toDateTimeString())->count(),
            "locations_annulees_count" => DB::table('locations')->where("statut","annulé")->count(),
            "users_count" => DB::table('users')->count(),
            "locations" => $loc ,
            "finances" => FluxFinance::orderBy('created_at','desc')->take(11)->get(),


        ];
    }

}
