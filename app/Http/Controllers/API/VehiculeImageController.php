<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Vehicule;
use Illuminate\Http\Request;
use DB;
use Log;

class VehiculeImageController extends Controller
{
    public function updateImages(Request $request)
    {
        try
        {

            //on commence la transaction
            DB::beginTransaction();

            $profileWasUpdated = false;
            $galleryWasUpdated = true;

            $message = "Aucune Modification transmise";
            $vehicule =Vehicule::with(['images'])->whereId($request->input('vehicule_id'))->first();
            $newImages = count($request->input('images'));

            //Image de mise en avant
            if($request->input('image_mise_en_avant')!="" || $request->input('image_mise_en_avant')!=null)
            {

                   $profileWasUpdated = true;
                   $message = "L'image principale a été mise à jour";
                   //on met a jour l image
                   $file = $request->input('image_mise_en_avant');
                    //on supprime l'image d'avant
                    if($vehicule->image_mise_en_avant!=null)
                    {
                        if(file_exists(storage_path('app/public/images/'.$vehicule->image_mise_en_avant)))
                        {
                            unlink(storage_path('app/public/images/'.$vehicule->image_mise_en_avant));
                        }
                    }
                   $vehicule->upload($file,$vehicule->getMime($file),$vehicule->id);

                 
            }
            //Galerie
            //le tableau de nouvelles images ne contient rien,le vehicule aussi 
            if(count($request->input('images'))==0 &&  count($vehicule->images)==0 )
            {
                //alors on ne fait rien
                $galleryWasUpdated = false;
            }
            //Si les nouvelles images sont vides et que le vehicule en a
            elseif(count($request->input('images'))==0 &&  count($vehicule->images)>0 )
            {

                
                // alors l'utilisateur souhaite supprimmer toutes les images du véhicule
                $paths = $vehicule->images->pluck('chemin');
                
                //on supprime les elements du vehicule dans la BDD
                $vehicule->images()->delete();
                //on supprime les images dans dossier images du stockage
                foreach($paths as $path)
                {
                    if(file_exists(storage_path('app/public/images/'.$path)))
                    {
                        unlink(storage_path('app/public/images/'.$path));
                    }
                }
                
                $message = "Galerie mise à jour";
            }
            //Si les nouvelles images sont présentes et que le vehicule n'en a pas
            elseif(count($request->input('images'))>0 &&  count($vehicule->images)==0 )
            {
                // alors l'utilisateur souhaite ajouter des images à un vehicule qui n'en a pas au départ
                foreach($request->input('images') as $image)
                {
                    $file = $image;
                    $vehicule->uploadGallery($file,$vehicule->getMime($file),$vehicule->id);
                    
                }
                $message = "Galerie mise à jour";
            }
            //sinon les 
            else
            {
                //given we have a b ancie
                //d
                //a est da
                $collection =collect($request->input('images')); 
                foreach($vehicule->images as $old)
                {
                    //Si l'image est dans les nouvelles image
                   if($collection->contains($old->chemin))
                   {
                       //on la retire des nouvelles images
                        $collection = $collection->filter(function ($value, $key) use($old) {
                            return $value != $old->chemin;
                        });
                   }
                   //sinon
                   else
                   {
                       //on la supprime
                       $old->delete();
                       //on supprime les images dans dossier images du stockage
                        if(file_exists(storage_path('app/public/images/'.$old->chemin)))
                        {
                            unlink(storage_path('app/public/images/'.$old->chemin));
                        } 
                   }
                }

                //on insere les nouvelles images                
                foreach($collection as $image)
                {
                    $file = $image;
                    $vehicule->uploadGallery($file,$vehicule->getMime($file),$vehicule->id);
                    
                }
                $message =  "La galerie a été mise à jour";
                
            }
            DB::commit();
            
            return response()->json(["profile"=> $vehicule->fresh()->image_mise_en_avant,'profileWasUpdated' => $profileWasUpdated,"message"=>$message,'galleryWasUpdated' => $galleryWasUpdated,'success' => true,'images'=>$vehicule->fresh()->load('images')->images],201);

     }
     catch(\Exception $e)
     {
             DB::rollback();
             Log::debug("something bad happened");
             Log::debug($e->getMessage());
             return response()->json(['success' => false,"message"=>$e->getMessage()],201);
     }
         
    }
}
