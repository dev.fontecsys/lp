<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' =>"required",
            'prenom' =>"required",
            'adresse' => "required",
            'telephone1' => "required",
            'piece_identite' => "required",
            'type_piece_identite' => "required|in:CNI,Passeport,Récepissé CNI,Permis de conduire,Autres",
            'date_naissance' => ["required","date",function($attribute,$value,$fails)
            {
                   $now = Carbon::now()->subYears(18);
                   $data = Carbon::parse($this->input('date_naissance'));
                   if($now->isBefore($data))
                   {
                       $fails('Le client doit avoir au moins 18 ans');
                   }
            }],
            'email' => "required|email",
        ];
    }

    public function messages()
    {
        return [

            'nom.required' =>"Le nom du chauffeur est requis",
            'prenom.required' =>"Le prénom du chauffeur est requis",
            'date_naissance.required'=>"La date de naissance est requise",
            'date_naissance.date'=>"La date doit avoir un format 'dd-mm-aaaa'",
            'adresse.required' =>"L'adresse du chauffeur est requise",
            'telephone1.required' =>"Le téléphone du chauffeur est requis",
            'type_piece_identite.required'=>"Le type de permis est requis",
            'piece_identite.required'=>"Une copie du permis est requise",
            'email.email'=>"L'email doit être d'un format valide",
            'email.require'=>"L'email est requis",
            'type_piece_identite.in'=>"le type de permisd peut être A,B,C,D ou E"

        ];
    }
}
