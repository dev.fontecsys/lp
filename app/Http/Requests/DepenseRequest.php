<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' =>"required",
            'montant' =>"required|numeric",
            'date_facturation' =>"required|date|date_format:Y-m-d",
            'vehicule_id' =>"required|exists:vehicules,id",
            'type_depense_id' =>"required|exists:type_depenses,id"

        ];
    }

    public function messages()
    {
        return[

            'vehicule_id.required' =>"Le vehicule est requis",
            'vehicule_id.exists' =>"Ce vehicule est inconnu",

            'date_facturation.required' =>"La date de facturation est requise",
            'date_facturation.date_format' =>"Le format  jj-mm-aaaa est requis",
            'date_facturation.date' =>"La date de facturation doit avoir le format d'une date",

            'type_depense_id.required' =>"Le type de dépense est requis",
            'type_depense_id.exists' =>"Ce type de dépense est inconnu",

            'montant.required' =>"Le montant est requis",
            'montant.numeric' =>"Le montant doit être un nombre",

            'description.required' =>"La description de la dépense est requise",


        ];
    }
}
