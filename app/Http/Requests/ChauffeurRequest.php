<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChauffeurRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' =>"required",
            'prenom' =>"required",
            'adresse' => "required",
            'telephone1' => "required",
            'permis_conduire' => "required",
            'type_permis_conduire' => "required|in:A,B,C,D,E",
            'nr_permis_conduire' => "required",
            'email' => "required|email",
        ];
    }


    public function messages()
    {
        return [

            'nom.required' =>"Le nom du chauffeur est requis",
            'prenom.required' =>"Le prénom du chauffeur est requis",
            'adresse.required' =>"L'adresse du chauffeur est requise",
            'telephone1.required' =>"Le téléphone du chauffeur est requis",
            'type_permis_conduire.required'=>"Le type de permis est requis",
            'permis_conduire.required'=>"Une copie du permis est requise",
            'nr_permis_conduire.required'=>"Le N° du permis est requis",
            'email.email'=>"L'email doit être d'un format valide",
            'email.require'=>"L'email est requis",
            'type_permis_conduire.in'=>"le type de permisd peut être A,B,C,D ou E"

        ];
    }
}
