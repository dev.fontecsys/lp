<?php

namespace App\Providers;

use App\Depense;
use App\Location;
use App\Observers\DepenseObserver;
use App\Observers\LocationObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        //Carbon::setLocale('fr');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        setLocale(LC_TIME, app()->getLocale());
        Carbon::setLocale(app()->getLocale());
        
        if (App::environment('local')) 
        {
            // The environment is local
            DB::enableQueryLog();
        }
        else
        {
            \Illuminate\Support\Facades\URL::forceScheme('https');
        }
        //\Illuminate\Support\Facades\URL::forceScheme('https');
        Schema::defaultStringLength(191);
        Depense::observe(DepenseObserver::class);
        Location::observe(LocationObserver::class);


       Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
        $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
        return new LengthAwarePaginator(
            $this->forPage($page, $perPage),
            $total ?: $this->count(),
            $perPage,
            $page,
            [
                'path' => LengthAwarePaginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    });
    
    }
}