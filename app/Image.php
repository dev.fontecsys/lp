<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class Image extends SModel
{
    use LogsActivity;

    protected static $logAttributes = ["chemin","vehicule_id"];
    protected static $logName = 'images';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;
}
