<?php

namespace App;

use App\Traits\CanUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Assurance extends SModel
{
    use CanUpload, LogsActivity;

    protected static $logAttributes = ['nr', 'assureur_id','date_dbt','date_fin'];
    protected static $logName = 'assurance';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;

    protected $storage_path ="public/documents/assurances";
    //protected $full_path = storage_path()."/app/public/documents/assurances/";

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé l'assurance <strong>{$this->nr}</strong> de chez  <strong>{ucFirst($this->assureur->nom)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé l'assurance n° <strong>{$this->nr}</strong> de chez  <strong>{ucFirst($this->assureur->nom)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié l'assurance n° <strong>{$this->nr}</strong> de chez  <strong>{ucFirst($this->assureur->nom)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié l'assurance n°  <strong>{$this->nr}</strong> de chez  <strong>{ucFirst($this->assureur->nom)}</strong> à la  voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté l'assurance n° <strong>{$this->nr}</strong> de chez  <strong>{ucFirst($this->assureur->nom)}</strong> à la voiture <strong>{$this->vehicule->libelle}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté l'assurance n° <strong>{$this->nr}</strong> à la voiture <strong>{$this->vehicule->libelle}</strong>";
        }
        
    }

    public function vehicule()
    {
        return $this->belongsTo("App\Vehicule");
    }

    public function assureur()
    {
        return $this->belongsTo("App\Assureur");
    }
}
