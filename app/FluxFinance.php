<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class FluxFinance extends SModel
{

    use LogsActivity;

    protected static $logAttributes = ['montant', 'financiable_id','vehicule_id','debut','financiable_type'];
    protected static $logName = 'flux-finance';
    protected static $logOnlyDirty = true;   
    protected static $submitEmptyLogs = false;
    protected $appends = ['type'];


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        $numero = "LC".Carbon::now()->year."-".$this->id;
        if($eventName=="deleted")
        {
            if($this->financiable_type=="App\Location")
            {
                $activity->as_yourself = "La location  {$this->financiable->numero} que vous avez supprimée à effacer une entrée  de  {$this->montant} F cfa dans les flux financiers";
                $activity->as_someone_else = "La location  {$this->financiable->numero}  que ".Auth::check() ? Auth::user()->name : "le système "." a supprimée à effacer une entrée  de  {$this->montant} F cfa dans les flux financiers";
            }
            else
            {
                $activity->as_yourself = "La dépense pour la voiture {$this->vehicule->libelle} que vous avez supprimée à effacer une sortie  de  {$this->montant} F cfa dans les flux financiers";
                $activity->as_someone_else = "La dépense pour la voiture   {$this->vehicule->libelle}  que ".Auth::check() ? Auth::user()->name : "le système "." à supprimée à effacer une entrée  de  {$this->montant} F cfa dans les flux financiers";
            }
        }
        elseif($eventName=="updated")
        {
            if($this->financiable_type=="App\Location")
            {
                $activity->as_yourself = "La location  {$this->financiable->numero} que vous avez modifiée à actualiser une entrée  de  {$this->montant} F cfa dans les flux financiers";
                $activity->as_someone_else = "La location   {$this->financiable->numero}  que ".Auth::check() ? Auth::user()->name : "le système "." a modifiée à actualiser une entrée  de  {$this->montant} F cfa dans les flux financiers";
            }
            else
            {
                $activity->as_yourself = "La dépense pour la voiture {$this->vehicule->libelle} que vous avez modifiée à actualiser une sortie  de  {$this->montant} F cfa dans les flux financiers";
                $activity->as_someone_else = "La dépense  pour la voiture  {$this->vehicule->libelle}  que ".Auth::check() ? Auth::user()->name : "le système "." a modifiée à actualiser une entrée  de  {$this->montant} F cfa dans les flux financiers";
            }
        }
        else
        {
            if($this->financiable_type=="App\Location")
            {
                $activity->as_yourself = "La location  {$this->financiable->numero} que vous avez créée à générer une entrée  de  {$this->montant} F cfa dans les flux financiers";
                $activity->as_someone_else = "La location  {$this->financiable->numero}  que ".Auth::check() ? Auth::user()->name : "le système "." a créée à générer une entrée  de  {$this->montant} F cfa dans les flux financiers";
            }
            else
            {
                $activity->as_yourself = "La dépense pour la voiture {$this->vehicule->libelle} que vous avez créée à générer une sortie  de  {$this->montant} F cfa dans les flux financiers";
                $activity->as_someone_else = "La dépense  pour la voiture {$this->vehicule->libelle}  que ".Auth::check() ? Auth::user()->name : "le système "." à créée à générer une entrée  de  {$this->montant} F cfa dans les flux financiers";
            }
           
        }
        
    }
    /**
     * Get the owning commentable model.
     */
    public function financiable()
    {
        return $this->morphTo();
    }

    public function vehicule()
    {
        return $this->belongsTo("App\Vehicule");
    }

    public function getTypeAttribute()
    {
        if ($this->financiable_type == "App\Depense") {
            return "Sortie";
        }
        if ($this->financiable_type == "App\Location") {
            return "Entrée";
        } else {
            return "Inconnu";
        }
    }


    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query->where(function($query)  use ($q){
            $query->orWhere('flux_finances.flux', 'LIKE', "%{$q}%")
            ->orWhere('flux_finances.montant', 'LIKE', "%{$q}%")
            ->orWhere('vehicules.libelle', 'LIKE', "%{$q}%")
            ->orWhere('locations.numero', 'LIKE', "%{$q}%")
            ;
        })->leftJoin('vehicules', 'vehicules.id', '=', 'flux_finances.vehicule_id')
          ->leftJoin('locations', 'locations.id', '=', 'flux_finances.financiable_id');
    }


    public function scopeVehiculeFilter($query, $q)
    {
        if ($q == null || $q=="all" || $q=="tous") return $query;
        return $query->where('flux_finances.vehicule_id',$q);
    }
    public function scopeTypeFluxFilter($query, $q)
    {
        if ($q == null || $q=="all" || $q=="tous") return $query;
        return $query->where('flux_finances.financiable_type',$q);
    }
    public function scopePeriode($query, $debut,$fin)
    {

        if ($debut == null || $fin==null) return $query;
        return $query->whereBetween('flux_finances.date_transaction',[$debut,$fin]);
    }


    public function scopeSearchPerMonthYear($query, $m,$a)
    {

        if ($m == null || $a==null) return $query;
        elseif($m!="tous" && $a!="tous")
        {
            $start_date = $a."-".sprintf("%02d", $m)."-01";

            $daysInThatMonth =Carbon::parse($start_date)->daysInMonth;
            $end_date = $a."-".sprintf("%02d", $m)."-".$daysInThatMonth;
    
            return $query
                ->where('flux_finances.date_transaction', '>=',$start_date.'')
                ->where(function($query)  use ($start_date,$end_date){
                    $query->whereBetween('flux_finances.date_transaction', [$start_date, $end_date]);
                });
        }
        elseif($m=="tous" && $a!="tous")
        {

    
            return $query
                ->whereYear('flux_finances.date_transaction', '=',$a.'');
              
        }
        elseif($m!="tous" && $a=="tous")
        {
            return $query
            ->whereMonth('flux_finances.date_transaction', '=',$m.'');
        }
        else
        return $query;

    }

    public function getResultOverTime($mois, $annee)
    {

        if( $this->financiable instanceof \App\Location)
        {
            $location =  $this->financiable;

          //Si la durée de la location s'etend sur le meme mois
          if($location->durationOnSameMonth)
          {
              //alors, on prend son montant
             return $location->montant;
          }
          else
          {
            //Sinon on recupere le montant généré  seulement dans le mois en question
            //pour cela,on prend le montant quotidien généré par la location durant sa durée
            $resultatParJour =  $location->detailsFinanciersJournaliers();
            //On recupere juste les resultat avec les  dates du mois en question
            $resultatParJour =  $resultatParJour->filter(function ($value, $key)  use ($mois)
            {
                $date = Carbon::parse($value['date']);

                return $date->month == $mois;
            });

            return $resultatParJour->sum('montant');
          }

        }
        else
        {
            return $this->montant*(1); 
        }

    }
}
