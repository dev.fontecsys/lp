@extends('layouts.app')

@section('content')
<login  inline-template v-cloak>
    <div class="login-box" >
      <div class="login-logo">
        <a href="javascript:void(0)">
        <img src="{{asset('img/logipa.png') }}" alt="" height="80" class="mb-2">

      </a>
      </div>
      <!-- /.login-logo -->
      <div class="card" style="border-radius: 1.5rem;">
        <div class="card-body login-card-body">
          <p class="login-box-msg">Connectez-vous pour lancer une session</p>

          <form method="POST"  @submit.prevent="onSubmit()"
                               @reset.prevent="reset()" 
                               @keydown="form.errors.clear($event.target.name)" 
                               @change="form.errors.clear($event.target.name)">
            <div class="input-group mb-3">
                <input placeholder="Entrez votre e-mail ou votre username" :class="['form-control form-control-sm', form.errors.has('identity')? ' is-invalid' : '']" name="identity" v-model="form.identity" type="text" />
                <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-envelope"></span>
                            </div>
                 </div>
                  <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('identity')" v-text="form.errors.get('identity')"></span>
            </div> 
            <div class="input-group mb-3">
                    <input placeholder="Entrez votre mot de passe" :class="['form-control form-control-sm', form.errors.has('password')? ' is-invalid' : '']" name="password" v-model="form.password" type="password" />
                    <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-lock"></span>
                        </div>
                    </div>
                 <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('password')" v-text="form.errors.get('password')"></span>
            </div>                
            <div class="align-items-center d-flex justify-content justify-content-between">
              <div class="">
                <div class="icheck-primary">
                  <input type="checkbox" id="remember">
                  <label for="remember">
                    Se souvenir de moi
                  </label>
                </div>
              </div>
              <!-- /.col -->
              <div class="">
                <button style="min-height: 38px;min-width:74px" type="submit" :class="['btn btn-primary btn-block mr-2',isLoading ? 'm-loader m-loader--sm m-loader--light m-loader--right' :'']" :disabled="form.errors.any()">@{{ isLoading ? "Login" : 'Login' }}</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
          <!-- /.social-auth-links -->

          <p class="mb-1">
            <a href="{{ route('password.request') }}">Mot de passe oublié ?</a>
          </p>
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
</login>

@endsection
