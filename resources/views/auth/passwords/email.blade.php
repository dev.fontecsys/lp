@extends('layouts.app')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Admin</b>| GEOLOC</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
      
    <div class="card-body login-card-body">
      <p class="login-box-msg " style="text-align:justify">Vous avez oublié votre mot de passe ? Pas de souci, vous pourrez recupérer votre mot de passe et pour cela:</p>

      <form method="POST" action="{{ route('password.email') }}">
                        @csrf
        <div class="input-group mb-3">
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="Veuillez rentrez votre mot e-mail"autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
          @enderror
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Demande un nouveau mot de passe</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-3 mb-1">
        <a href="{{ route('login') }}">Connexion</a>
      </p>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
@endsection
