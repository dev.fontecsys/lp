<?php

use Illuminate\Database\Seeder;

class DepenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type_depenses= ["Assurance","Visite techniques","Frais de garage","Carburant","Salaire","Autres"];

        foreach( $type_depenses as $tv)
        {
            factory('App\TypeDepense')->create(["libelle"=>$tv]);
        }

    }
}
