<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\Vehicule::class, function (Faker $faker) {

    
    $marque_id = function(){ return factory('App\Marque')->create()->id;};
    return [
        "libelle"=>$faker->sentence($nbWords = 2, $variableNbWords = true),
        'nbr_porte' => $faker->randomElement([3,5]),
        'nbr_place' => $faker->randomElement([2,4,5,8,12]),
        'boite_vitesse' => $faker->randomElement(["manuelle","automatique"]),
        'couleur' => $faker->word,
        'kilometrage' => $faker->numberBetween($min = 0, $max = 12500) ,

        'type_motorisation_id' => function(){ return factory('App\TypeMotorisation')->create()->id;},
        'type_vehicule_id' => function(){ return factory('App\TypeVehicule')->create()->id;},

        'marque_id' => $marque_id,
        'modele_id' => function() use($marque_id) { return factory('App\Modele')->create(["marque_id"=>$marque_id])->id;},
        'type_permis_id'=>function(){ return factory('App\TypePermis')->create()->id;},


        'tarif'=>$faker->numberBetween($min = 25000, $max = 70000) ,
        'plaque_immatriculation'=>Str::random(10),
        'nr_chassis'=>Str::random(10),
        'date_mise_en_circulation'=>$faker->date(),
        'annee_mise_en_circulation'=>$faker->numberBetween($min = 2000, $max = 2020) ,

        'consommation_au_100_km'=>$faker->randomFloat($nbMaxDecimals = NULL, $min = 5, $max = 13),
        'volume_coffre'=>$faker->randomFloat($nbMaxDecimals = NULL, $min = 40, $max = 100),
        'volume_reservoir'=>$faker->randomFloat($nbMaxDecimals = NULL, $min = 50, $max = 120),
        'largeur'=>$faker->randomFloat($nbMaxDecimals = NULL, $min = 2, $max = 2.89),
        'longueur'=>$faker->randomFloat($nbMaxDecimals = NULL, $min = 3, $max = 4.59),
        'poids'=>$faker->randomFloat($nbMaxDecimals = NULL, $min = 600, $max = 2398),
        'hauteur'=>$faker->randomFloat($nbMaxDecimals = NULL, $min = 2, $max = 2.3)

    ];
});
$factory->define(App\Chauffeur::class, function (Faker $faker) {

    return [
        "nom" => $faker->firstName(),
        "prenom" => $faker->lastname,
        "permis_conduire" => "default_permis.jpg",
        "nr_permis_conduire" => Str::random(12),
        "type_permis_conduire" => $faker->randomElement(["A", "B",'C',"D","E"]),
        'telephone1' => $faker->phoneNumber,
        'telephone2' => $faker->phoneNumber,
        'email' => $faker->unique($reset = true)->safeEmail,
        'adresse' => $faker->streetAddress." , Libreville" ,

    ];
});

$factory->define(App\Client::class, function (Faker $faker) {

    $date1 = Carbon::now();
    $date = $faker->date($format = 'Y-m-d', $max =$date1->subYears(18)->toDateString());
    $expl = explode('-',"".$date);

    return [
        "nom" => $faker->lastname,
        "prenom" => $faker->lastname,
        "type_piece_identite" => $faker->randomElement(['CNI','Passeport','Récepissé CNI','Autres']),
        "piece_identite" => Str::random(12),
        'pays_id' => function(){ return factory('App\Pays')->create()->id;},
        'nr_permis_conduire' => function(){ return factory('App\Permis')->create()->numero;},

        'date_naissance'=>$date,

        'telephone1' => $faker->phoneNumber,
        'telephone2' => $faker->phoneNumber,
        'email' => $faker->unique($reset = true)->safeEmail,
        //Adresse
        'adresse' => $faker->streetAddress  ,
        'quartier' => $faker->word, // secret
        'bp' => $faker->postCode,
        'ville' => $faker->city,



    ];
});


$factory->define(App\Location::class, function (Faker $faker) {

    $date = $faker->date($format = 'Y-m-d h:m:i ', $max = 'now');
    $carbonDate= Carbon::parse($date);
    $ve = factory('App\Vehicule')->create();
    $client = factory('App\Client')->create();
    $days = $faker->numberBetween($min = 2, $max = 20);

    $client_est_chauffeur = $faker->randomElement([true,false]);
    $frais = $faker->randomElement([true,false]);
    $tarif = $ve->tarif;

    return [
        "date_dbt" => $date,
        "date_fin" => $carbonDate->addDays($days)->toDateTimeString(),
        'client_id' => $client->id,
        'vehicule_id' => $ve->id,
        'numero' => "lc-".Carbon::now()->year.''.Str::random(5),
        'montant'=> ($days)*$tarif + ($frais) ? 5000 : 0 ,
        'tarif'=> $tarif,
        'nbr_jour'=> $days,
        "client_est_chauffeur"=>$client_est_chauffeur,
        "chauffeur_id"=>!$client_est_chauffeur ? function(){ return factory('App\Chauffeur')->create()->id;} : null ,
        "zone_location"=>$faker->city,
        "livraison_effective"=>$frais,
        "statut_payement_id"=>function(){ return factory('App\StatutPayement')->create()->id;} ,

        "frais_livraison"=>$frais ? 5000 : 0,
        "longue_duree"=>$faker->randomElement([true,false]),
        "avec_chauffeur"=>!$client_est_chauffeur,
        "niveau_carburant"=>$faker->randomElement(["1/2","1/4","3/4","plein"]),
        "lavage_carburant"=>10000,
        'nom_chauffeur'=> $client_est_chauffeur ? $client->nomComplet : $faker->firstName()." ".$faker->lastName,
        'nom_chauffeur_2'=> $faker->randomElement([null,$faker->firstName()." ".$faker->lastName])
    ];


});

$factory->define(App\Depense::class, function (Faker $faker) {
    return [

        'description' => $faker->sentence,
        'date_facturation' => $faker->date(),
        'montant'=> $days = $faker->numberBetween($min = 10000, $max = 150000),
        'type_depense_id' => function(){ return factory('App\TypeDepense')->create()->id;},
        'vehicule_id' => function(){ return factory('App\Vehicule')->create()->id;},

    ];


});
