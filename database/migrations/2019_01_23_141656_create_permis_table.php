<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permis', function (Blueprint $table) 
        {
            $table->bigIncrements('id');
            $table->string('numero')->unique();
            $table->date('date_emission');
            $table->unsignedBigInteger('type_permis_id')->nullable();
            $table->unsignedBigInteger('pays_id')->nullable();
            $table->boolean('international')->default(false);

            $table->timestamps();

            $table->foreign('type_permis_id')->references('id')->on('type_permis')->onDelete('set null');
            $table->foreign('pays_id')->references('id')->on('pays')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permis');
    }
}
