<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assurances', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string("nr");
            $table->date("date_dbt");
            $table->string("date_fin");
            $table->string("fichier",1000)->nullable();
            $table->unsignedBigInteger('vehicule_id');
            $table->unsignedBigInteger('assureur_id')->nullable();

            $table->timestamps();

            $table->foreign('vehicule_id')->references('id')->on('vehicules')->onDelete('cascade');
            $table->foreign('assureur_id')->references('id')->on('assureurs')->onDelete('set null');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assurances');
    }
}
