<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('vehicule_id');

            $table->boolean('avec_chauffeur')->default(false);
            $table->boolean("client_est_chauffeur")->default(true);
            $table->boolean("livraison_effective")->default(false);
            $table->boolean('longue_duree')->default(false);
            $table->enum('niveau_carburant', ["1/2", "1/4","3/4",'plein'])->default('plein');
            $table->enum('statut', ['en cours','terminé','annulé'])->default('terminé');


            $table->datetime('date_dbt');
            $table->datetime('date_fin');
            $table->index(['date_dbt','date_fin']);
            $table->double('nbr_jour');
            $table->string('nom_chauffeur')->nullable();
            $table->string('nom_chauffeur_2')->nullable();
            $table->double('montant')->nullable();
            $table->double('tarif')->nullable();

            $table->double('lavage_carburant')->default(10000);
            $table->double("frais_livraison")->default(5000);
            $table->text("observation",200)->nullable();
            $table->string("zone_location",200)->nullable();

            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('vehicule_id')->references('id')->on('vehicules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
