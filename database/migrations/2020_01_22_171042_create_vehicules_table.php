
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicules', function (Blueprint $table) {

            $table->bigIncrements('id')->index();
            $table->string('libelle')->nullable();
            $table->index(['libelle','plaque_immatriculation']);
            $table->unsignedBigInteger('marque_id')->nullable();
            $table->unsignedBigInteger('modele_id')->nullable();
            $table->unsignedBigInteger('type_vehicule_id')->nullable();
            $table->unsignedBigInteger('type_permis_id')->nullable();

            $table->unsignedBigInteger('nbr_place');
            $table->unsignedBigInteger('nbr_porte')->nullable();
            $table->string('couleur');
            $table->string('image_mise_en_avant')->nullable();


            //tarifs
            $table->double('tarif');
            $table->double('kilometrage');

            //unicités
            $table->string('plaque_immatriculation')->unique();
            $table->string('nr_chassis')->nullable();

            $table->date('date_mise_en_circulation')->nullable();
            $table->string('annee_mise_en_circulation')->nullable();
            $table->double('consommation_au_100_km')->nullable();

            //moteurs
            $table->unsignedBigInteger('type_motorisation_id')->nullable();
            $table->enum('boite_vitesse', ["manuelle", "automatique"])->default("automatique");

            //mesures physiques
            $table->double('volume_coffre')->nullable();
            $table->double('volume_reservoir')->nullable();
            $table->double('largeur')->nullable();
            $table->double('longueur')->nullable();
            $table->double('poids')->nullable();
            $table->double('hauteur')->nullable();

            $table->timestamps();

            $table->foreign('type_motorisation_id')->references('id')->on('type_motorisations')->onDelete('set null');
            $table->foreign('marque_id')->references('id')->on('marques')->onDelete('set null');
            $table->foreign('modele_id')->references('id')->on('modeles')->onDelete('set null');

            $table->foreign('type_vehicule_id')->references('id')->on('type_vehicules')->onDelete('set null');
            $table->foreign('type_permis_id')->references('id')->on('type_permis')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicules');
    }
}
